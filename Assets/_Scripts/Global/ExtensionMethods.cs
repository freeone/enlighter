using UnityEngine;

public static class ExtensionMethods
{
	public static Vector3 GetRandomPointOnRadius(this Transform transform, float radius)
	{
		return new Vector3(Random.Range(-1f, 1f), Random.Range(-1f, 1f), 0).normalized * radius + transform.position;
	}
}
