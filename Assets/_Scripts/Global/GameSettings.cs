using UnityEngine;
using UnityEngine.UI;

public class GameSettings : MonoBehaviour
{
    private void Awake()
    {
        Screen.SetResolution(500, 900, false);
        Application.targetFrameRate = 60;

        //QualitySettings.maxQueuedFrames = 1;
        QualitySettings.vSyncCount = 1;

        //Time.captureFramerate = Screen.currentResolution.refreshRate;
    }
}
