using UnityEngine;

public class Updater : MonoBehaviour
{
    [SerializeField] private CameraFollow _cameraFollow;
    [SerializeField] private Starfield[] _starfields;
    [SerializeField] private MapGenerator _map;
    [SerializeField] private CoinSpawner _coinSpawner;
    [SerializeField] private EnemySpawner _enemySpawner;
    [SerializeField] private PlayerInput _playerInput;
    [SerializeField] private BasicMover _playerMover;
    [SerializeField] private UserInterfaceManager _managerUI;

    public AppState CurrentAppState;

    private void Awake()
    {
        CurrentAppState = AppState.menu;
    }

    private void Update()
    {
        if (CurrentAppState == AppState.menu)
        {
            _map.UpdateMap();
            UpdateStarfields();
        }
        else if (CurrentAppState == AppState.game)
            UpdateInGame();
    }

    //current updates out of this script: warTail
    private void UpdateInGame()
    {
        _managerUI.UpdateUIManager();
        _playerInput.PlayerInputUpdate();
        _playerMover.UpdateMover();

        _enemySpawner.SpawnerUpdate();
        UpdateSeekers();

        _coinSpawner.UpdateCoins();
        _map.UpdateMap();
        UpdateStarfields();
        _cameraFollow.CameraUpdate();
    }

    private void UpdateStarfields()
    {
        foreach (Starfield field in _starfields)
        {
            field.StarfieldUpdate();
        }
    }

    private void UpdateSeekers()
    {
        foreach (SeekingEnemy seeker in _enemySpawner.AliveSeekers)
        {
            seeker.UpdateSeeker();
            seeker.Mover.UpdateMover();
        }
    }
}

public enum AppState { menu, game }