using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.Events;

public class StateTransitions : MonoBehaviour
{
    [SerializeField] private Image _innerRing;
    [SerializeField] private SpriteRenderer _playerRenderer;
    [SerializeField] private Player _player;
    [SerializeField] private Transform _playerHolder;
    [SerializeField] private RectTransform _centerPoint;
    [SerializeField] private GameObject _trails;

    [SerializeField] private Updater _updater;
    [SerializeField] private BonusSpawner _bonusSpawner;
    [SerializeField] private UserInterfaceManager _uiManager;
    [SerializeField] private CanvasGroup _mainMenuGroup;
    [SerializeField] private CanvasGroup _gameGroup;
    [SerializeField] private CanvasGroup _gameOverGroup;

    private Camera _camera;
    private float _defaultCameraSize = 9f;
    private float _scaleFactor = 1.4f;
    private Vector3[] _corners;
    private float _mainMenuCameraSize;
    private Vector3 _mainMenuCameraOffset;

    public static StateTransitions Instance { get; private set; }

    private void Awake()
    {
        Instance = this;
    }

    private void OnEnable()
    {
        _player.PlayerDied += OnPlayerDied;
    }

    private void OnDisable()
    {
        _player.PlayerDied -= OnPlayerDied;
    }

    private void Start()
    {
        _camera = Camera.main;
        _corners = new Vector3[4];

        _innerRing.rectTransform.GetWorldCorners(_corners);
        float ringWidth = _corners[2].x - _corners[1].x;
        float playerHeight = _playerRenderer.bounds.size.y;

        float finalScaleFactor = playerHeight / ringWidth * _scaleFactor;

        _mainMenuCameraSize = _defaultCameraSize * finalScaleFactor;
        _camera.orthographicSize = _mainMenuCameraSize;

        Vector2 offset = (_innerRing.rectTransform.position - _centerPoint.position) * -finalScaleFactor;
        _mainMenuCameraOffset = new Vector3(offset.x, offset.y - playerHeight/2f, -10);

        _camera.transform.position = _player.transform.position + _mainMenuCameraOffset;
    }

    public void MainToGame(float transitionTime)
    {
        _player.DisableCollider();
        _camera.DOOrthoSize(_defaultCameraSize, transitionTime).OnComplete(EnterGameState);
        _camera.transform.DOMove(new Vector3 (_playerRenderer.transform.position.x, _playerRenderer.transform.position.y, -10), transitionTime);
        _mainMenuGroup.DOFade(0, transitionTime / 2f).OnComplete(() =>
        {
            _uiManager.ChangeScreens(_mainMenuGroup.gameObject, _gameGroup.gameObject);
            _gameGroup.DOFade(1, transitionTime / 2f).OnComplete(() =>
            {
                _playerRenderer.DOFade(0.5f, 0.375f).SetLoops(4, LoopType.Yoyo).OnComplete(() =>
                {
                    _trails.SetActive(true);
                    _player.EnableCollider();
                    _playerRenderer.DOFade(1, 0.01f);
                });
            });
        });
    }

    private void OnPlayerDied() // = game to gameOver
    {
        _updater.CurrentAppState = AppState.menu;
        ResetStates();
        _trails.SetActive(false);

        _gameGroup.alpha = 0;
        _gameOverGroup.alpha = 1;
        _uiManager.ChangeScreens(_gameGroup.gameObject, _gameOverGroup.gameObject);
        _uiManager.EnableGameOverButtons();
    }

    public void GameOverToGame(float transitionTime, UnityAction restartAction)
    {
        _player.DisableCollider();
        _gameOverGroup.DOFade(0, transitionTime / 2f).OnComplete(() =>
        {
            _uiManager.ChangeScreens(_gameOverGroup.gameObject, _gameGroup.gameObject);
            _gameGroup.DOFade(1, transitionTime / 2f);
            _playerHolder.DOLocalRotate(Vector3.zero, transitionTime / 2f);
            _playerRenderer.DOFade(1, transitionTime / 2f).OnComplete(() =>
            {
                restartAction?.Invoke();
                _updater.CurrentAppState = AppState.game;
                //1.5 sec untouchable
                _playerRenderer.DOFade(0.5f, 0.375f).SetLoops(4, LoopType.Yoyo).OnComplete(() => 
                {
                    _trails.SetActive(true);
                    _player.EnableCollider();
                    _playerRenderer.DOFade(1, 0.01f);
                });
            });
        });
    }

    public void GameOverToMenu(float transitionTime)
    { 
        _gameOverGroup.DOFade(0, transitionTime / 4f).OnComplete(() =>
        {
            _mainMenuGroup.alpha = 0;
            _uiManager.ChangeScreens(_gameOverGroup.gameObject, _mainMenuGroup.gameObject);
            _uiManager.EnableMenuButtons();
            _mainMenuGroup.DOFade(1, transitionTime * 0.75f);
        });
        _playerRenderer.DOFade(1, transitionTime);
        _playerHolder.DORotate(Vector3.zero, transitionTime);
        _camera.DOOrthoSize(_mainMenuCameraSize, transitionTime);
        _camera.transform.DOMove(_player.transform.position + _mainMenuCameraOffset, transitionTime);
    }

    private void EnterGameState()
    {
        LaunchSpawners();

        _player.ResetPlayer();
        _updater.CurrentAppState = AppState.game;
    }

    public void ResetStates()
    {
        _player.ResetPlayer();
        EnemySpawner.Instance.ResetEnemySpawner();
        CoinSpawner.Instance.ResetCoinSpawner();
        _bonusSpawner.ResetBonusSpawner();
    }

    public void LaunchSpawners()
    {
        EnemySpawner.Instance.LoadLevel(0);
        CoinSpawner.Instance.SpawnStartCoins();
        _bonusSpawner.StartSpawnBonuses();
    }
}
