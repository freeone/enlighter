using UnityEngine;
using UnityEngine.Pool;
using UnityEngine.UI;

public class Pointer : MonoBehaviour
{
    [SerializeField] private CanvasGroup _canvasGroup;
    [SerializeField] private RectTransform _transform;

    private float _borderSize = 0;
    private Vector2 _targetPositionScreenPoint;
    private Vector3 _vectorToTarget;
    private float _targetVectorToAngle;
    private ObjectPool<Pointer> _pool;

    public CanvasGroup Group => _canvasGroup;

    public void UpdatePosition(Vector3 targetPosition, RectTransform canvasRect)
    {
        _targetPositionScreenPoint = Camera.main.WorldToScreenPoint(targetPosition);

        _transform.rotation = Quaternion.Euler(0, 0, GetDesiredRotation(targetPosition, canvasRect));

        if (_targetPositionScreenPoint.x <= 0) _targetPositionScreenPoint.x = 0;
        else if (_targetPositionScreenPoint.x >= Screen.width) _targetPositionScreenPoint.x = canvasRect.rect.width;
        else _targetPositionScreenPoint.x = _targetPositionScreenPoint.x/Screen.width * canvasRect.rect.width;
        
        if (_targetPositionScreenPoint.y <= 0) _targetPositionScreenPoint.y = 0;
        else if (_targetPositionScreenPoint.y >= Screen.height) _targetPositionScreenPoint.y = canvasRect.rect.height;
        else _targetPositionScreenPoint.y = _targetPositionScreenPoint.y / Screen.height * canvasRect.rect.height;

        _targetPositionScreenPoint.x -= canvasRect.rect.width/2;
        _targetPositionScreenPoint.y -= canvasRect.rect.height/2;

        _transform.localPosition = _targetPositionScreenPoint;
    }

    private float GetDesiredRotation(Vector3 targetPosition, RectTransform canvasRect)
    {
        if (_targetPositionScreenPoint.x > _borderSize && _targetPositionScreenPoint.x < Screen.width - _borderSize)
        {
            if (_targetPositionScreenPoint.y < 0) return 180;
            else return 0;
        }

        if (_targetPositionScreenPoint.y > _borderSize && _targetPositionScreenPoint.y < Screen.height - _borderSize)
        {
            if (_targetPositionScreenPoint.x < 0) return 90;
            else return 270;
        }

        _vectorToTarget = targetPosition - _transform.position;
        _vectorToTarget.z = 0;

        _targetVectorToAngle = Vector3.Angle(new Vector3(0.0f, 1.0f, 0.0f), _vectorToTarget);
        if (_vectorToTarget.x < 0.0f) _targetVectorToAngle = 360.0f - _targetVectorToAngle;

        return 360f - _targetVectorToAngle;
    }

    public void EnableRenderers(float newAlpha)
    {
        _canvasGroup.alpha = newAlpha;
    }

    public void DisableRenderers()
    {
        _canvasGroup.alpha = 0;
    }

    public void SetPool(ObjectPool<Pointer> pool)
    {
        _pool = pool;
    }

    public void Release()
    {
        DisableRenderers();
        _pool.Release(this);
        gameObject.SetActive(false);
    }
}