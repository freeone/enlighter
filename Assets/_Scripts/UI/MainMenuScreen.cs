using UnityEngine;
using UnityEngine.UI;

public class MainMenuScreen : MonoBehaviour
{
    [SerializeField] private Button _playButton;
    [SerializeField] private GameScreen _gameScreen;

    public void OnPlayButton()
    {
        _playButton.interactable = false;
        StateTransitions.Instance.MainToGame(2);
    }

    public void EnableButtons()
    {
        _playButton.interactable = true;
    }
}