using UnityEngine;
using UnityEngine.UI;

public class GameScreen : MonoBehaviour
{
    [SerializeField] private Player _player;
    [SerializeField] private Text _hp;
    [SerializeField] private Text _armor;
    [SerializeField] private Text _coins;
    [SerializeField] private GameObject _pausePanel;
    [SerializeField] private Text _changeModeText;

    private string _finiteMode = "CHANGE MODE\n[NOW: FINITE]";
    private string _infiniteMode = "CHANGE MODE\n[NOW: INFINITE]";

    private void OnEnable()
    {
        _player.HealthChanged += OnHealthChanged;
        _player.CoinsChanged += OnCoinsChanged;
        _player.ArmorChanged += OnArmorChanged;
    }

    private void OnDisable()
    {
        _player.HealthChanged -= OnHealthChanged;
        _player.CoinsChanged -= OnCoinsChanged;
        _player.ArmorChanged -= OnArmorChanged;
    }

    private void OnHealthChanged(int health)
    {
        _hp.text = health.ToString();
    }

    private void OnArmorChanged(int armor)
    {
        _armor.text = armor.ToString();
    }

    private void OnCoinsChanged(int coins)
    {
        _coins.text = coins.ToString();
    }

    public void PauseGame()
    {
        Time.timeScale = 0;
        _pausePanel.SetActive(true);
        _changeModeText.text = _player.InfiniteMode ? _infiniteMode : _finiteMode;
    }

    public void UnpauseGame()
    {
        _pausePanel.SetActive(false);
        Time.timeScale = 1;
    }

    public void ChangeGameMode()
    {
        _player.InfiniteMode = !_player.InfiniteMode;
        _changeModeText.text = _player.InfiniteMode ? _infiniteMode : _finiteMode;
    }
}