using UnityEngine;

public class UserInterfaceManager : MonoBehaviour
{
    [SerializeField] private Player _player;
    [SerializeField] private CanvasGroup _gameGroup;
    [SerializeField] private GameOverScreen _gameOverScreen;
    [SerializeField] private CoinSpawner _coinSpawner;
    [SerializeField] private EnemySpawner _enemySpawner;
    [SerializeField] private BonusSpawner _bonusSpawner;
    [SerializeField] private MainMenuScreen _mainMenuScreen;

    public void UpdateUIManager()
    {
        if (Input.GetKeyDown(KeyCode.R)) OnRestartClicked();
    }

    private void OnEnable()
    {
        _gameOverScreen.RestartClicked += OnRestartClicked;
    }

    private void OnDisable()
    {
        _gameOverScreen.RestartClicked -= OnRestartClicked;
    }

    public void ChangeScreens(GameObject screenToDisable, GameObject screenToEnable)
    {
        screenToDisable.SetActive(false);
        screenToEnable.SetActive(true);
    }

    public void EnableMenuButtons()
    {
        _mainMenuScreen.EnableButtons();
    }

    public void EnableGameOverButtons()
    {
        _gameOverScreen.EnableButtons();
    }

    private void OnRestartClicked()
    {
        StateTransitions.Instance.ResetStates();
        StateTransitions.Instance.LaunchSpawners();
    }
}