using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class GameOverScreen : MonoBehaviour
{
    [SerializeField] private Button _restartButton;
    [SerializeField] private Button _menuButton;

    public event UnityAction RestartClicked;

    public void RestartGame()
    {
        DisableButtons();
        StateTransitions.Instance.GameOverToGame(2, RestartClicked);
    }

    public void GoToMenu()
    {
        DisableButtons();
        StateTransitions.Instance.GameOverToMenu(2);
    }

    private void DisableButtons()
    {
        _restartButton.interactable = false;
        _menuButton.interactable = false;
    }

    public void EnableButtons()
    {
        _restartButton.interactable = true;
        _menuButton.interactable = true;
    }
}
