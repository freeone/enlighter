using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Pool;

public class EnemySpawner : MonoBehaviour
{
    [SerializeField] private float _spawnDistance;
    [SerializeField] private LevelWaves[] _levelWaves;
    [SerializeField] private SeekingEnemy _seekerTemplate;
    [SerializeField] private ParticleSystem _enemyDestroyParticles;
    [SerializeField] private ParticleSystem _enemyRewardParticles;

    private int _currentLevel;
    private Wave[] _currentLevelWaves;
    private float _timeElapsed;
    private Dictionary<int, float> _startTimes;
    private List<int> _startTimesToDelete;
    private Vector3 _randomSpawnPosition;
    private Transform _player;
    private Wave _nextWave;
    private List<Coroutine> _spawnCoroutines;

    private ObjectPool<SeekingEnemy> _seekerPool;
    private List<SeekingEnemy> _aliveSeekers;

    public List<SeekingEnemy> AliveSeekers => _aliveSeekers;
    public static EnemySpawner Instance { get; private set; }

    private void Awake()
    {
        Instance = this;
        _startTimes = new Dictionary<int, float>();
        _startTimesToDelete = new List<int>();
        _spawnCoroutines = new List<Coroutine>();
        _seekerPool = new ObjectPool<SeekingEnemy>(CreateSeeker, actionOnRelease: OnSeekerReleases);
        _aliveSeekers = new List<SeekingEnemy>();
    }

    private void Start()
    {
        _player = Player.Instance.transform;
    }

    public void SpawnerUpdate()
    {
        _timeElapsed += Time.deltaTime;

        if (_startTimes.Count > 0)
        {
            foreach (var startTime in _startTimes)
            {
                if (_timeElapsed >= startTime.Value)
                {
                    _spawnCoroutines.Add(StartCoroutine(StartWave(_currentLevelWaves[startTime.Key])));
                    _currentLevelWaves[startTime.Key].SetCoroutine(_spawnCoroutines[_spawnCoroutines.Count - 1]); // get just added coroutine and assign it to the wave
                    _startTimesToDelete.Add(startTime.Key); // can't delete from Dictionary while iterating
                }
            }

            if (_startTimesToDelete.Count > 0)
            {
                foreach (int waveId in _startTimesToDelete) _startTimes.Remove(waveId);
                _startTimesToDelete.Clear();
            }
        }
    }

    public void LoadLevel(int level)
    {
        _startTimes.Clear();
        _currentLevel = level;
        _currentLevelWaves = _levelWaves[_currentLevel].Waves;
        _timeElapsed = 0;

        for (int i = 0; i < _currentLevelWaves.Length; i++) 
        {
            if (_currentLevelWaves[i].UseStartTime)
            {
                _startTimes.Add(i, _currentLevelWaves[i].StartTime);
            }
        }        
    }

    private IEnumerator StartWave(Wave wave)
    {
        Debug.Log($"Wave started. Enemy count: {wave.EnemyCount}, Start time: {wave.StartTime} : {_timeElapsed}");
        WaitForSeconds spawnDelay = new WaitForSeconds(wave.SpawnDelay);
        wave.ResetDeadEnemies();

        if (wave.EnemyClass == EnemyType.Seeker)
        {
            for (int i = 0; i < wave.EnemyCount; i++)
            {
                _randomSpawnPosition = _player.GetRandomPointOnRadius(_spawnDistance);
                SeekingEnemy spawned = _seekerPool.Get();
                _aliveSeekers.Add(spawned);
                spawned.Spawn(_player, wave, _randomSpawnPosition);
                spawned.SetInitialRotation();
                
                yield return spawnDelay;
            }
        }

        _spawnCoroutines.Remove(wave.WaveCoroutine);

        yield break;
    }

    public void WaveStartsWave(int waveToStart)
    {
        _nextWave = _currentLevelWaves[waveToStart];
        if (_nextWave.StartTime > 0) _startTimes.Add(waveToStart, _nextWave.StartTime + _timeElapsed);
    }

    private SeekingEnemy CreateSeeker()
    {
        var seeker = Instantiate(_seekerTemplate, transform);
        seeker.SetPool(_seekerPool);
        seeker.Init();
        return seeker;
    }

    private void OnSeekerReleases(SeekingEnemy seeker)
    {
        _aliveSeekers.Remove(seeker);
        seeker.gameObject.SetActive(false);
    }

    public void EmitDestroyParticles(Vector3 position, int count)
    {
        _enemyDestroyParticles.transform.position = position;
        _enemyDestroyParticles.Emit(count);
    }

    public void EmitRewardParticles(Vector3 position, int count)
    {
        _enemyRewardParticles.transform.position = position;
        _enemyRewardParticles.Emit(count);
    }

    public void ResetEnemySpawner()
    {
        _startTimesToDelete.Clear();
        foreach (var coroutine in _spawnCoroutines)
        {
            StopCoroutine(coroutine);
        }
        
        _spawnCoroutines.Clear();

        while (_aliveSeekers.Count > 0)
        {
            _aliveSeekers[0].Die(false);
        }

        _enemyDestroyParticles.Clear();
        _enemyRewardParticles.Clear();
    }
}