using UnityEngine;

public abstract class Enemy : MonoBehaviour
{
    [SerializeField] private EnemyCollider _collider;

    protected Transform Target;
    protected Wave _currentWave;

    public virtual void Init()
    {
        _collider.Init(this);
    }

    public virtual void Spawn(Transform target, Wave wave, Vector3 newPosition)
    {
        Target = target;
        _currentWave = wave;
        _collider.EnableShield();
        transform.position = newPosition;
        gameObject.SetActive(true);
    }

    public virtual void Die(bool naturally)
    {
        if (naturally) _currentWave.EnemyDied();
        Destroy(gameObject);
    }
}