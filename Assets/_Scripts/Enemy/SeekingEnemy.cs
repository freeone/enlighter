using UnityEngine;
using UnityEngine.Pool;

[RequireComponent(typeof(BasicMover))]
public class SeekingEnemy : Enemy
{
    [SerializeField] private float _targetOffset;
    [SerializeField] private BasicMover _mover;
    
    private Vector3 _vectorToTarget;
    private float _targetVectorToAngle;
    private float _desiredAngle;
    private float _angleDifference;
    private ObjectPool<SeekingEnemy> _pool;

    public BasicMover Mover => _mover;

    public void UpdateSeeker()
    {
        _desiredAngle = GetDesiredRotation();

        _angleDifference = transform.rotation.eulerAngles.z - _desiredAngle;
        if (_angleDifference < 0) _angleDifference += 360f;

        if (_angleDifference != 0)
        {
            if (Mathf.Abs(_angleDifference) > _mover.RotationSpeed * Time.deltaTime)
            {
                if (_angleDifference < 180) _mover.RotateClockWise();
                else _mover.RotateCounterclockwise();
            }
            else transform.rotation = Quaternion.Euler(0, 0, _desiredAngle);
        }
    }

    public void SetInitialRotation()
    {
        transform.rotation = Quaternion.Euler(0, 0, GetDesiredRotation());
    }

    private float GetDesiredRotation()
    {        
        _vectorToTarget = (Target.position + Target.transform.up * _targetOffset) - transform.position;     // this behaviour can be implemented better
        _targetVectorToAngle = Vector3.Angle(new Vector3(0.0f, 1.0f, 0.0f), _vectorToTarget);               // by using tranform.up with Vector3.Angle
        if (_vectorToTarget.x < 0.0f) _targetVectorToAngle = 360.0f - _targetVectorToAngle;             

        return 360f - _targetVectorToAngle;
    }

    public void SetPool(ObjectPool<SeekingEnemy> pool)
    {
        _pool = pool;
    }

    public override void Die(bool naturally)
    {
        if (naturally) _currentWave.EnemyDied();
        _pool.Release(this);
    }
}