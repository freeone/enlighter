using UnityEngine;

public class EnemyCollider : MonoBehaviour
{
    [SerializeField] private int _bodyDamage;
    [SerializeField] private SpriteRenderer _renderer;
    [SerializeField] private Color _shieldEnabledColor;
    [SerializeField] private Color _shieldDisabledColor;
    [SerializeField] private TrailRenderer _trail;
    [SerializeField] private Gradient _shieldEnabledGradient;
    [SerializeField] private Gradient _shieldDisabledGradient;

    private bool _shieldEnabled;
    private Enemy _enemy;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.TryGetComponent(out Player player))
        {
            player.TakeDamage(_bodyDamage);
            EnemySpawner.Instance.EmitDestroyParticles(transform.position, 5);
            TryToDie(false);
        }
        else if (collision.TryGetComponent(out WarTail warTail))
        {
            warTail.EmitParticles(3);
            EnemySpawner.Instance.EmitRewardParticles(transform.position, 1);
            TryToDie(false);
        }
    }

    public void Init(Enemy enemy)
    {
        _enemy = enemy;
    }

    public void EnableShield()
    {
        _shieldEnabled = true;
        _renderer.color = _shieldEnabledColor;
        _trail.colorGradient = _shieldEnabledGradient;
    }

    public void DisableShield()
    {
        _shieldEnabled = false;
        _renderer.color = _shieldDisabledColor;
        _trail.colorGradient = _shieldDisabledGradient;
    }

    public bool TryToDie(bool byObstacle)
    {
        if (!_shieldEnabled)
        {
            if (byObstacle)
            { 
                EnemySpawner.Instance.EmitDestroyParticles(transform.position, 3);
                EnemySpawner.Instance.EmitRewardParticles(transform.position, 1);
            }
            _trail.Clear();
            _enemy.Die(true);
            return true;
        }
        else return false;
    }
}