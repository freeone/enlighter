using System;
using UnityEngine;

[CreateAssetMenu(fileName = "New level", menuName = "Levels/Create new level", order = 51)]
public class LevelWaves : ScriptableObject
{
    [SerializeField] private int _id;
    [SerializeField] private Wave[] _waves;

    public Wave[] Waves => _waves;
}

[Serializable]
public class Wave
{
    [SerializeField] private int _id;
    [SerializeField] private EnemyType _enemyClass;
    [SerializeField] private int _enemyCount;
    [SerializeField] private float _spawnDelay;
    [Tooltip("First wave should always use startTime")]
    [SerializeField] private bool _useStartTime;
    [Tooltip("You can use startTime to setup delay after previous wave")]
    [SerializeField] private float _startTime;
    [SerializeField] private int _nextWaveToStart;
    [SerializeField] private int _coinsSpawnedAfterWave;

    private int _enemiesDied;
    private Coroutine _waveCoroutine;

    public EnemyType EnemyClass => _enemyClass;
    public int EnemyCount => _enemyCount;
    public float SpawnDelay => _spawnDelay;
    public bool UseStartTime => _useStartTime;
    public float StartTime => _startTime;
    public Coroutine WaveCoroutine => _waveCoroutine;

    public void ResetDeadEnemies()
    {
        _enemiesDied = 0;
    }

    public void EnemyDied()
    {
        _enemiesDied++;
        if (_enemiesDied == _enemyCount)
        {
            Debug.Log("Wave " + _id + " cleared!");
            if (_coinsSpawnedAfterWave > 0) CoinSpawner.Instance.SpawnCoins(_coinsSpawnedAfterWave);
            if (_nextWaveToStart > -1) EnemySpawner.Instance.WaveStartsWave(_nextWaveToStart);
        }
    }

    public void SetCoroutine(Coroutine coroutine)
    {
        _waveCoroutine = coroutine;
    }
}

public enum EnemyType
{
    Seeker = 1
}
