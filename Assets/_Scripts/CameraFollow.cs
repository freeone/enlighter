using UnityEngine;

[RequireComponent(typeof(Camera))]
public class CameraFollow : MonoBehaviour
{
    [SerializeField] private Transform _target;
    //[SerializeField] private float _offset;
    //[SerializeField] private float _smoothTime;

    private Vector3 _desiredPosition;
    //private Vector3 _velocity = Vector3.zero;

    public void CameraUpdate()
    {
        //_desiredPosition = new Vector3(_target.transform.position.x, _target.transform.position.y, transform.position.z) + _target.transform.up * _offset;
        //transform.position = Vector3.SmoothDamp(transform.position, _desiredPosition, ref _velocity, _smoothTime);

        _desiredPosition.x = _target.position.x;
        _desiredPosition.y = _target.position.y;
        _desiredPosition.z = -10;
        transform.position = _desiredPosition;
    }
}