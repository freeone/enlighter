using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Pool;

public class BonusSpawner : MonoBehaviour
{
    [SerializeField] private float _spawnPeriod;
    [SerializeField] private float _spawnDistance;
    [SerializeField] private Bonus _bonusTemplate;
    [SerializeField] private Transform _player;
    [SerializeField] private float _lifeTime;
    [SerializeField] private ParticleSystem _healthParticleSystem;
    [SerializeField] private ParticleSystem _armorParticleSystem;

    [SerializeField] private Color _healthColor;
    [SerializeField] private float _healthValue;
    [SerializeField] private Color _speedColor;
    [SerializeField] private float _speedValue;
    [SerializeField] private Color _rotationColor;
    [SerializeField] private float _rotationValue;
    [SerializeField] private Color _defenceColor;
    [SerializeField] private float _defenceValue;
    [SerializeField] private Color _wartailColor;

    private Dictionary<BonusType, Color> _bonusColors;
    private Dictionary<BonusType, float> _bonusValues;

    private WaitForSeconds _waitSpawn;
    private Coroutine _spawnCoroutine;
    private WaitForSeconds _waitDestroy;

    private ObjectPool<Bonus> _bonusPool;
    private Dictionary<Bonus, Coroutine> _bonusDestroyers;
    private BonusType _spawningType;

    public ObjectPool<Bonus> BonusPool => _bonusPool;
    public Dictionary<Bonus, Coroutine> BonusDestroyers => _bonusDestroyers;

    private void Awake()
    {
        _bonusColors = new Dictionary<BonusType, Color>()
        {
            { BonusType.Health, _healthColor },
            { BonusType.Movement, _speedColor },
            { BonusType.Rotation, _rotationColor },
            { BonusType.Wartail, _wartailColor },
            { BonusType.Defence, _defenceColor }
        };
        _bonusValues = new Dictionary<BonusType, float>()
        {
            { BonusType.Health, _healthValue },
            { BonusType.Movement, _speedValue },
            { BonusType.Rotation, _rotationValue },
            { BonusType.Wartail, 0 },
            { BonusType.Defence, _defenceValue }
        };
    }

    private void Start()
    {
        _bonusPool = new ObjectPool<Bonus>(CreateBonus, actionOnRelease: OnBonusReleases);
        _bonusDestroyers = new Dictionary<Bonus, Coroutine>();

        if (_spawnPeriod < 1) _spawnPeriod = 1; 
        _waitSpawn = new WaitForSeconds(_spawnPeriod);
        if (_lifeTime < 1) _lifeTime = 1;
        _waitDestroy = new WaitForSeconds(_lifeTime);
    }

    public void StartSpawnBonuses()
    {
        _spawnCoroutine = StartCoroutine(SpawnLoop());
    }

    private IEnumerator SpawnLoop()
    {
        while(true)
        {
            SpawnBonus();
            yield return _waitSpawn;
        }
    }

    private void SpawnBonus()
    {
        _spawningType = (BonusType)Random.Range(0, 5);
        Bonus spawned = _bonusPool.Get();
        spawned.SetupBonus(_spawningType, _bonusValues[_spawningType], _bonusColors[_spawningType]);
        spawned.transform.position = _player.GetRandomPointOnRadius(_spawnDistance);
        _bonusDestroyers.Add(spawned, StartCoroutine(DestroyBonusDelayed(spawned)));
        spawned.gameObject.SetActive(true);
    }

    private IEnumerator DestroyBonusDelayed(Bonus bonus)
    {
        yield return _waitDestroy;
        _bonusPool.Release(bonus);
        _bonusDestroyers.Remove(bonus);
        yield break;
    }

    private Bonus CreateBonus()
    {
        var bonus = Instantiate(_bonusTemplate, transform);
        bonus.SetSpawner(this);
        return bonus;
    }

    private void OnBonusReleases(Bonus bonus)
    {
        bonus.gameObject.SetActive(false);
    }

    public void ResetBonusSpawner()
    {
        StopCoroutine(_spawnCoroutine);
        foreach (var destroyer in _bonusDestroyers)
        {
            StopCoroutine(destroyer.Value);
            _bonusPool.Release(destroyer.Key);
        }

        _bonusDestroyers.Clear();
        _healthParticleSystem.Clear();
        _armorParticleSystem.Clear();
    }

    public void EmitParticles(int count, BonusType type, Vector3 position)
    {
        if (type == BonusType.Health)
        {
            _healthParticleSystem.transform.position = position;
            _healthParticleSystem.Emit(count);
        }
        else if (type == BonusType.Defence)
        {
            _armorParticleSystem.transform.position = position;
            _armorParticleSystem.Emit(count);
        }
    }
}