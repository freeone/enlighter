using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class Bonus : MonoBehaviour
{
    [SerializeField] private SpriteRenderer _renderer;

    private BonusType _bonusType;
    private float _bonusAmount;
    private BonusSpawner _spawner;

    public BonusType TypeOfBonus => _bonusType;
    public float BonusAmount => _bonusAmount;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.TryGetComponent<Player>(out Player player))
        {
            if (_bonusType == BonusType.Health)
            {
                _spawner.EmitParticles(Mathf.FloorToInt(_bonusAmount / 5), _bonusType, transform.position);
            }
            else if (_bonusType == BonusType.Defence)
            {
                _spawner.EmitParticles(Mathf.FloorToInt(_bonusAmount / 5), _bonusType, transform.position);
            }
            else if (_bonusType == BonusType.Movement || _bonusType == BonusType.Rotation || _bonusType == BonusType.Wartail)
            {
                player.Buffs.AddBuff(this);
            }

            _spawner.StopCoroutine(_spawner.BonusDestroyers[this]);
            _spawner.BonusDestroyers.Remove(this);
            _spawner.BonusPool.Release(this);
        }
    }

    public void SetupBonus(BonusType type, float amount, Color color)
    {
        _bonusType = type;
        _bonusAmount = amount;
        _renderer.sprite = SpriteHolder.Instance.BonusSprites[type];
        _renderer.color = color;
    }

    public void SetSpawner(BonusSpawner spawner)
    {
        _spawner = spawner;
    }
}

public enum BonusType { Health, Movement, Rotation, Wartail, Defence, Gold = 100 }