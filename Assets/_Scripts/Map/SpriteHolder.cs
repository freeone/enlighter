using System.Collections.Generic;
using UnityEngine;

public class SpriteHolder : MonoBehaviour
{
    [SerializeField] private List<Sprite> _obstacleSprites;

    [SerializeField] private Sprite _healthBonusSprite;
    [SerializeField] private Sprite _speedBonusSprite;
    [SerializeField] private Sprite _rotationBonusSprite;
    [SerializeField] private Sprite _wartailBonusSprite;
    [SerializeField] private Sprite _defenceBonusSprite;

    private Dictionary<BonusType, Sprite> _bonusSprites;

    public List<Sprite> ObstacleSprites => _obstacleSprites;
    public Dictionary<BonusType, Sprite> BonusSprites => _bonusSprites;
    public static SpriteHolder Instance { get; set; }

    private void Awake()
    {
        Instance = this;

        _bonusSprites = new Dictionary<BonusType, Sprite>()
        {
            { BonusType.Health, _healthBonusSprite },
            { BonusType.Movement, _speedBonusSprite },
            { BonusType.Rotation, _rotationBonusSprite },
            { BonusType.Wartail, _wartailBonusSprite },
            { BonusType.Defence, _defenceBonusSprite }
        };
    }
}