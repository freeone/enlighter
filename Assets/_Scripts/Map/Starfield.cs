using UnityEngine;

[RequireComponent(typeof(ParticleSystem))]
public class Starfield : MonoBehaviour
{
	[SerializeField] private int _maxStars = 25;
	[SerializeField] private float _starSize = 0.1f;

	[Range(0.05f, 0.5f)]
	[SerializeField] private float _starSizeRange = 0.5f;
	[SerializeField] private float _fieldWidth = 12f;
	[SerializeField] private float _fieldHeight = 15f;

	[Range(0f, 1f)]
	[SerializeField] private float _parallaxFactor = 0f;
	[SerializeField] private bool _colorize = false;

	private ParticleSystem _particles;
	private ParticleSystem.Particle[] _stars;
	private float _xOffset;
	private float _yOffset;
	private Vector3 _starPosition;
	private Vector3 _starfieldPosition;
	private Transform _camera;

	private void Awake()
	{
		_particles = GetComponent<ParticleSystem>();
		_stars = new ParticleSystem.Particle[_maxStars];

		_xOffset = _fieldWidth * 0.5f;			// Offset the coordinates to distribute the spread
		_yOffset = _fieldHeight * 0.5f;			// around the object's center

		for (int i = 0; i < _maxStars; i++)
		{
			float randomNormalizedSize = Random.Range(1f - _starSizeRange, 1f + _starSizeRange);        // Randomize star size within parameters
			float scaledColor = _colorize ? randomNormalizedSize - _starSizeRange : 1f;					// If coloration is desired, color is based on size

			_stars[i].position = GetRandomInRectangle(_fieldWidth, _fieldHeight) + transform.position;
			_stars[i].startSize = _starSize * randomNormalizedSize;
			_stars[i].startColor = new Color(1f, scaledColor, scaledColor, 1f);
		}
		_particles.SetParticles(_stars, _stars.Length);		// Write data to the particle system
	}

    private void Start()
    {
		_camera = Camera.main.transform;
	}

    public void StarfieldUpdate()
	{
		for (int i = 0; i < _maxStars; i++)
		{
			_starPosition = _stars[i].position + transform.position;

			if (_starPosition.x < (_camera.position.x - _xOffset))
			{
				_starPosition.x += _fieldWidth;
			}
			else if (_starPosition.x > (_camera.position.x + _xOffset))
			{
				_starPosition.x -= _fieldWidth;
			}

			if (_starPosition.y < (_camera.position.y - _yOffset))
			{
				_starPosition.y += _fieldHeight;
			}
			else if (_starPosition.y > (_camera.position.y + _yOffset))
			{
				_starPosition.y -= _fieldHeight;
			}

			_stars[i].position = _starPosition - transform.position;
		}
		_particles.SetParticles(_stars, _stars.Length);

		_starfieldPosition = _camera.position * _parallaxFactor;
		_starfieldPosition.z = 0;
		transform.position = _starfieldPosition;
	}

	Vector3 GetRandomInRectangle(float width, float height)
	{
		float x = Random.Range(0, width);
		float y = Random.Range(0, height);
		return new Vector3(x - _xOffset, y - _yOffset, 0);
	}
}