using UnityEngine;

public class Coin : MonoBehaviour
{
    [SerializeField] private int _value;
    [SerializeField] private float _lifetime;
    [SerializeField] private SpriteRenderer _renderer;

    public Pointer CoinPointer;
    public float Lifetime => _lifetime;
    public SpriteRenderer Renderer => _renderer;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.TryGetComponent<Player>(out Player player))
        {
            player.ChangeCoinsCount(Mathf.RoundToInt(Random.Range(_value * 0.5f, _value * 1.5f)));
            Disappear();
        }
    }

    public void Disappear()
    {
        CoinPointer.Release();
        CoinSpawner.Instance.RemoveCoinFromLists(this);
        Destroy(gameObject);
    }
}