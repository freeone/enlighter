using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Pool;

public class CoinSpawner : MonoBehaviour
{
    [SerializeField] private Transform _player;
    [SerializeField] private float _spawnDistance;
    [SerializeField] private Coin _templateSmall;
    [SerializeField] private Coin _templateMedium;
    [SerializeField] private Coin _templateBig;
    [SerializeField] private Pointer _pointerTemplate;
    [SerializeField] private Canvas _canvas;

    private List<Coin> _aliveCoins = new List<Coin>();
    private List<float> _lifeTimes = new List<float>();
    private List<Coin> _coinsToRemove = new List<Coin>();
    private Coin _spawnedCoin;
    private Vector3 _randomSpawnPosition;
    private ObjectPool<Pointer> _pointerPool;
    private float _newAlpha;
    private Coroutine _spawnCoroutine;
    private bool _spawnCoroutineDone;
    private RectTransform _canvasTransform;

    public static CoinSpawner Instance { get; private set; }

    private void Awake()
    {
        Instance = this;
        _pointerPool = new ObjectPool<Pointer>(CreatePointer, OnGetPointer);
    }

    private void Start()
    {
        _canvasTransform = _canvas.GetComponent<RectTransform>();
    }

    public void UpdateCoins()
    {
        for (int i = 0; i < _aliveCoins.Count; i++)
        {
            if (_lifeTimes[i] < 0.1f)
            {
                _coinsToRemove.Add(_aliveCoins[i]);
                continue;
            }
            else
            {
                _newAlpha = _lifeTimes[i] / _aliveCoins[i].Lifetime;
                _lifeTimes[i] -= Time.deltaTime;
            }

            if (_aliveCoins[i].Renderer.isVisible) _aliveCoins[i].CoinPointer.DisableRenderers();
            else
            {
                _aliveCoins[i].CoinPointer.UpdatePosition(_aliveCoins[i].transform.position, _canvasTransform) ;
                _aliveCoins[i].CoinPointer.EnableRenderers(_newAlpha);
            }
        }

        if (_coinsToRemove.Count > 0)
        {
            foreach (Coin coin in _coinsToRemove)
            {
                coin.Disappear();
            }

            _coinsToRemove.Clear();
        }
    }

    public void SpawnCoins(int count)
    {
        for (int i = 0; i < count; i++)
        {
            _randomSpawnPosition = _player.GetRandomPointOnRadius(_spawnDistance);
            float chance = Random.Range(0, 20);

            if (chance < 5) _spawnedCoin = Instantiate(_templateSmall, _randomSpawnPosition, Quaternion.identity, transform);
            else if (chance < 9) _spawnedCoin = Instantiate(_templateMedium, _randomSpawnPosition, Quaternion.identity, transform);
            else _spawnedCoin = Instantiate(_templateBig, _randomSpawnPosition, Quaternion.identity, transform);

            _spawnedCoin.CoinPointer = _pointerPool.Get();

            _aliveCoins.Add(_spawnedCoin);
            _lifeTimes.Add(_spawnedCoin.Lifetime);
        }
    }

    public void RemoveCoinFromLists(Coin coin)
    {
        int toRemove = _aliveCoins.IndexOf(coin);
        _aliveCoins.RemoveAt(toRemove);
        _lifeTimes.RemoveAt(toRemove);
    }

    public void SpawnStartCoins()
    {
        _spawnCoroutineDone = false;
        _spawnCoroutine = StartCoroutine(SpawnCoinsDelayed(1, 1));
    }

    private IEnumerator SpawnCoinsDelayed(float delay, int count)
    {
        yield return new WaitForSeconds(delay);
        SpawnCoins(count);
        _spawnCoroutineDone = true;
        yield break;
    }

    private Pointer CreatePointer()
    {
        var pointer = Instantiate(_pointerTemplate, _canvas.transform);
        pointer.SetPool(_pointerPool);
        return pointer;
    }

    private void OnGetPointer(Pointer pointer)
    {
        pointer.gameObject.SetActive(true);
    }

    public void ResetCoinSpawner()
    {
        while(_aliveCoins.Count > 0)
        {
            _aliveCoins[0].Disappear();
        }

        _aliveCoins.Clear();
        _lifeTimes.Clear();
        _coinsToRemove.Clear();

        if (!_spawnCoroutineDone) StopCoroutine(_spawnCoroutine);
    }
}