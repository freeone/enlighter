using UnityEngine;

public class MapGenerator : MonoBehaviour
{
    [SerializeField] private Transform _playerTransform;
    [SerializeField] private int _mapRadius; // 0: center only (1), 1: 9 total, 2: 25 total
    [SerializeField] private float _cellSize;
    [SerializeField] private Obstacle _obstacleTemplate;

    private const float MinimalDistanceToPlayer = 1.5f; // value to check just after map generation

    private GriddedPosition _currentGridCenter;
    private GriddedPosition _previousGridCenter;
    private MapCell[] _mapCells;

    private void Start()
    {
        SetCurrentGridCenter();
        _previousGridCenter = _currentGridCenter;

        InitializeMapCells();
    }

    public void UpdateMap()
    {
        SetCurrentGridCenter();
        ResetCellPositions();
    }

    private void SetCurrentGridCenter()
    {
        _currentGridCenter = new GriddedPosition(Mathf.FloorToInt(_playerTransform.position.x / _cellSize), Mathf.FloorToInt(_playerTransform.position.y / _cellSize));
    }

    private void InitializeMapCells()
    {
        _mapCells = new MapCell[(int)Mathf.Pow((_mapRadius * 2) + 1, 2)];

        int cellsSpawned = 0;
        for (int i = _currentGridCenter.X - _mapRadius; i < (_currentGridCenter.X + _mapRadius + 1); i++)
        {
            for (int j = _currentGridCenter.Y - _mapRadius; j < (_currentGridCenter.Y + _mapRadius + 1); j++)
            {
                _mapCells[cellsSpawned] = new MapCell(new GriddedPosition(i,j), _cellSize, Instantiate(_obstacleTemplate, transform));
                CheckAndCorrectInitalPosition(_playerTransform, _mapCells[cellsSpawned], MinimalDistanceToPlayer);

                cellsSpawned++;
            }
        }        
    }

    private void CheckAndCorrectInitalPosition(Transform player, MapCell mapCellOfObstacle, float minimalDistanceToPlayer)
    {
        while(Vector2.Distance(player.position, mapCellOfObstacle.Obstacle.transform.position) < minimalDistanceToPlayer)
        {
            Debug.Log("Too close to player!");
            mapCellOfObstacle.ResetObstaclePosition();
        }
    }

    private void ResetCellPositions()
    {
        bool gridMoved = false;

        if (_currentGridCenter.X != _previousGridCenter.X) //player moved horizontally (and changed the cell)
        {
            MoveCellsHorizontally(_currentGridCenter.X > _previousGridCenter.X);
            gridMoved = true;
        }

        if (_currentGridCenter.Y != _previousGridCenter.Y)
        {
            MoveCellsVertically(_currentGridCenter.Y > _previousGridCenter.Y);
            gridMoved = true;
        }

        if (gridMoved) _previousGridCenter = _currentGridCenter;
    }

    private void MoveCellsHorizontally(bool right)
    {
       sbyte sign = right ? (sbyte)1 : (sbyte)-1;

        for (int i = 0; i < _mapCells.Length; i++) //check all map cells
        {
            if (_mapCells[i].Position.X == (_previousGridCenter.X - sign * _mapRadius)) // find all cells in (true)left column of the grid
            {
                _mapCells[i].ChangeCellPosition(_currentGridCenter.X + sign * _mapRadius, _mapCells[i].Position.Y); // move them to the (true)right column
            }
        }
    }

    private void MoveCellsVertically(bool up)
    {
        sbyte sign = up ? (sbyte)1 : (sbyte)-1;

        for (int i = 0; i < _mapCells.Length; i++)
        {
            if (_mapCells[i].Position.Y == (_previousGridCenter.Y - sign * _mapRadius))
            {
                _mapCells[i].ChangeCellPosition(_mapCells[i].Position.X, _currentGridCenter.Y + sign * _mapRadius);
            }
        }
    }
}

public struct GriddedPosition
{
    public int X;
    public int Y;

    public GriddedPosition(int horizontalPosition, int verticalPosition)
    {
        X = horizontalPosition;
        Y = verticalPosition;
    }
}