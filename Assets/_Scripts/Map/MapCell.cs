using UnityEngine;

public class MapCell
{
    private GriddedPosition _position;
    private float _size;
    private Vector2 _startPosition = new Vector2();
    private Vector2 _endPosition = new Vector2();
    private Obstacle _obstacle;
    private float _obstacleBorderOffset = 0.05f;

    public GriddedPosition Position => _position;
    public Obstacle Obstacle => _obstacle;

    public MapCell(GriddedPosition position, float cellSize, Obstacle obstacle)
    {
        _position = position;
        _size = cellSize;
        _obstacle = obstacle;

        SetBorders(ref _startPosition, ref _endPosition, _position, _size);

        ResetObstaclePosition();
        _obstacle.ResetObstacle();
    }

    public void ChangeCellPosition(int newX, int newY)
    {
        _position.X = newX;
        _position.Y = newY;

        SetBorders(ref _startPosition, ref _endPosition, _position, _size);
        ResetObstaclePosition();
        _obstacle.ResetObstacle();
    }

    public void ResetObstaclePosition()
    {
        _obstacle.transform.position = GenerateObstaclePosition(_startPosition, _endPosition, _size);
    }

    private void SetBorders(ref Vector2 startPosition, ref Vector2 endPosition, GriddedPosition currentPosition, float size)
    {
        startPosition = new Vector2(currentPosition.X * size, currentPosition.Y * size);
        endPosition = new Vector2(startPosition.x + size, startPosition.y + size);
    }

    private Vector2 GenerateObstaclePosition(Vector2 startPosition, Vector2 endPosition, float size)
    {
        float x = Random.Range(startPosition.x + size * _obstacleBorderOffset, endPosition.x - size * _obstacleBorderOffset);
        float y = Random.Range(startPosition.y + size * _obstacleBorderOffset, endPosition.y - size * _obstacleBorderOffset);
        return new Vector2(x, y);
    }
}