using UnityEngine;

public class Obstacle : MonoBehaviour
{
    [SerializeField] private Vector2 _sizeRange;
    [SerializeField] private SpriteRenderer _spriteRenderer;
    [SerializeField] private ObstacleCollider _collider;

    private float _size;

    public void ResetObstacle()
    {
        _size = Random.Range(_sizeRange.x, _sizeRange.y);
        transform.localScale = new Vector3(_size, _size, 1);
        transform.rotation = Quaternion.Euler(0, 0, Random.Range(0, 360));

        if (SpriteHolder.Instance.ObstacleSprites.Count > 0)
            _spriteRenderer.sprite = SpriteHolder.Instance.ObstacleSprites[Random.Range(0, SpriteHolder.Instance.ObstacleSprites.Count)];

        _collider.EnableCollider();
    }
}