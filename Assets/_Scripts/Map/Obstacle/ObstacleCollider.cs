using System.Collections;
using UnityEngine;

[RequireComponent(typeof(Collider2D), typeof(SpriteRenderer))]
public class ObstacleCollider : MonoBehaviour
{
    [SerializeField] private float _rechargeTimeInSeconds;
    [Range(0f, 1f)]
    [SerializeField] private float _rechargingAlpha;

    private Collider2D _collider;
    private SpriteRenderer _renderer;
    private WaitForSeconds _rechargeTime;
    private Color _defaultColor;
    private Color _rechargingColor;

    private void Awake()
    {
        _collider = GetComponent<Collider2D>();
        _renderer = GetComponent<SpriteRenderer>();
        _defaultColor = _renderer.color;
        _rechargingColor = new Color(_defaultColor.r, _defaultColor.g, _defaultColor.b, _rechargingAlpha);
        _rechargeTime = new WaitForSeconds(_rechargeTimeInSeconds);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.TryGetComponent(out Player player))
        {
            player.CollideObstacle();
        }

        if (collision.TryGetComponent(out EnemyCollider enemy))
        {
            if (enemy.TryToDie(true)) StartCoroutine(Recharge());
        }
    }

    private IEnumerator Recharge()
    {
        _collider.enabled = false;
        _renderer.color = _rechargingColor;
        yield return _rechargeTime;
        EnableCollider();

        yield break;
    }

    public void EnableCollider()
    {
        _renderer.color = _defaultColor;
        _collider.enabled = true;
    }
}