using UnityEngine;
using UnityEngine.Events;

public class Player : MonoBehaviour
{
    [SerializeField] private int _maxHealth;
    [SerializeField] private int _minHealthToSurviveCollision;
    [SerializeField] private int _maxArmor;
    [SerializeField] private PlayerBuffs _playerBuffs;
    [SerializeField] private ParticleSystem _lostArmorParticles;
    [SerializeField] private ParticleSystem _lostArmorSubParticles;
    [SerializeField] private ParticleSystem _lostHealthParticles;
    [SerializeField] private ParticleSystem _lostHealthSubParticles;
    [SerializeField] private SpriteRenderer _renderer;
    [SerializeField] private Collider2D _collider;

    private int _health;
    private int _armor;
    private int _coins;

    public event UnityAction<int> HealthChanged;
    public event UnityAction<int> ArmorChanged;
    public event UnityAction<int> CoinsChanged;
    public event UnityAction PlayerDied;

    public bool InfiniteMode { get; set; }
    public PlayerBuffs Buffs => _playerBuffs;
    public static Player Instance { get; private set; }

    private void Awake()
    {
        Instance = this;
        InfiniteMode = false;
    }

    private void Start()
    {
        ResetPlayer();
    }

    public void TakeDamage(int damage)
    {
        if (damage <= _armor)
        {
            _armor -= damage;
            _lostArmorParticles.Emit(1);
            _lostArmorSubParticles.Emit(Mathf.Max(1, damage / 5));
            ArmorChanged?.Invoke(_armor);
        }
        else if (_health + _armor > damage)
        {
            if (_armor > 0)
            {
                _lostArmorParticles.Emit(1);
                _lostArmorSubParticles.Emit(Mathf.Max(1, _armor / 5));
                _lostHealthParticles.Emit(1);
                _lostHealthSubParticles.Emit(Mathf.Max(1, (damage - _armor) / 5));

                _health -= damage - _armor;
                _armor = 0;

                HealthChanged?.Invoke(_health);
                ArmorChanged?.Invoke(_armor);
            }
            else
            {
                _health -= damage;
                _lostHealthParticles.Emit(1);
                _lostHealthSubParticles.Emit(Mathf.Max(1, damage / 5));
                HealthChanged?.Invoke(_health);
            }
        }
        else if (_health + _armor <= damage)
        {
            if (_armor > 0)
            {
                _lostArmorParticles.Emit(1);
                _lostArmorSubParticles.Emit(Mathf.Max(1, _armor / 5));
            }
            _lostHealthParticles.Emit(1);
            _lostHealthSubParticles.Emit(Mathf.Max(1, _health / 5));

            _health = 0;
            _armor = 0;
            if (!InfiniteMode) Die();

            HealthChanged?.Invoke(_health);
            ArmorChanged?.Invoke(_armor);
        }
    }

    public void GetHeal(int healAmount)
    {
        if (_health + healAmount >= _maxHealth)
            _health = _maxHealth;
        else _health += healAmount;
        HealthChanged?.Invoke(_health);
    }

    public void AddArmor(int armorToAdd)
    {
        if (_armor + armorToAdd >= _maxArmor)
            _armor = _maxArmor;
        else _armor += armorToAdd;
        ArmorChanged?.Invoke(_armor);
    }

    public void CollideObstacle()
    {
        if (_health < _minHealthToSurviveCollision) TakeDamage(1000); // armor wont help you if you're low
        else if (_armor > 0) TakeDamage(_armor); // but it can save you if you're fine
        else TakeDamage(_health / 3); // no armor = take damage
    }

    public void ChangeCoinsCount(int difference)
    {
        if (_coins + difference < 0) _coins = 0;
        else _coins += difference;

        CoinsChanged?.Invoke(_coins);
    }

    private void Die()
    {
        PlayerDied?.Invoke();
        _renderer.color = new Color(_renderer.color.r, _renderer.color.g, _renderer.color.b, 0);
    }

    public void ResetPlayer()
    {
        _health = _maxHealth;
        _armor = 0;
        _coins = 0;
        _playerBuffs.ResetBuffs();
        _lostArmorParticles.Clear();
        _lostHealthParticles.Clear();

        HealthChanged?.Invoke(_health);
        ArmorChanged?.Invoke(_armor);
        CoinsChanged?.Invoke(_coins);
    }

    public void DisableCollider()
    {
        _collider.enabled = false;
    }

    public void EnableCollider()
    {
        _collider.enabled = true;
    }
}