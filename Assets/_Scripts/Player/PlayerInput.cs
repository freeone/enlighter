using UnityEngine;

public class PlayerInput : MonoBehaviour
{
    private BasicMover _mover;
    private Touch _touch;

    private void Awake()
    {
        _mover = GetComponent<BasicMover>();
    }

    public void PlayerInputUpdate()
    {
        if (Input.GetKey(KeyCode.A))
        {
            _mover.RotateCounterclockwise();
        }
        if (Input.GetKey(KeyCode.D))
        {
            _mover.RotateClockWise();
        }

        if (Input.touchCount > 0)
        {
            _touch = Input.GetTouch(0);
            if (_touch.position.y < Screen.height * 0.5f)
            {
                if (_touch.position.x < Screen.width / 2) _mover.RotateCounterclockwise();
                else _mover.RotateClockWise();
            }
        }
    }
}
