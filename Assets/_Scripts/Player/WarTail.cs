using UnityEngine;

public class WarTail : MonoBehaviour
{
    [SerializeField] private Transform _tailCollider;
    [SerializeField] private TrailRenderer _trailRenderer;
    [SerializeField] private float _colliderPosition;
    [SerializeField] private ParticleSystem _particles;

    private int _trailPartsCount;

    private void Update()
    {
        _trailPartsCount = _trailRenderer.positionCount;

        if (_trailPartsCount > 0) 
            _tailCollider.position = _trailRenderer.GetPosition(Mathf.FloorToInt(_trailPartsCount / _colliderPosition));
    }

    public void EmitParticles(int count)
    {
        _particles.Emit(count); //they wont be destroyed after restart (no need in fact)
    }
}