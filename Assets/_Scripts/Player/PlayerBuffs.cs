using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBuffs : MonoBehaviour
{
    [SerializeField] private float _buffDuration;
    [SerializeField] private BasicMover _playerMover;
    [SerializeField] private GameObject _rotationWings;
    [SerializeField] private GameObject _defaultTail;
    [SerializeField] private GameObject _warTail;

    private Dictionary<BonusType, int> _buffs;
    private WaitForSeconds _waitBuffDuration;
    private Dictionary<BonusType, Coroutine> _activeRemovers;

    private void Start()
    {
        ResetBuffArray();
        _activeRemovers = new Dictionary<BonusType, Coroutine>();

        if (_buffDuration == 0) _buffDuration = 5;
        _waitBuffDuration = new WaitForSeconds(_buffDuration);
    }

    public void AddBuff(Bonus bonus)
    {
        if (_buffs[bonus.TypeOfBonus] == 0)
        {
            if (bonus.TypeOfBonus == BonusType.Movement)
            {
                _playerMover.BonusAdditiveMovement = bonus.BonusAmount;
            }
            else if (bonus.TypeOfBonus == BonusType.Rotation)
            {
                _playerMover.BonusMultiplicativeRotation = bonus.BonusAmount;
                _rotationWings.SetActive(true);
            }
            else if (bonus.TypeOfBonus == BonusType.Wartail)
            {
                _warTail.SetActive(true);
                _defaultTail.SetActive(false);
            }

            _buffs[bonus.TypeOfBonus] = 1;
            _activeRemovers.Add(bonus.TypeOfBonus, StartCoroutine(DelayedBuffRemove(bonus.TypeOfBonus, _waitBuffDuration)));
        }
        else _buffs[bonus.TypeOfBonus]++;
    }

    private IEnumerator DelayedBuffRemove(BonusType type, WaitForSeconds waitDuration)
    {
        while (_buffs[type] > 0)
        {
            yield return waitDuration;
            _buffs[type]--;
        }

        RemoveBuff(type);
        yield break;
    }

    private void RemoveBuff(BonusType type)
    {
        if (type == BonusType.Movement)
        {
            _playerMover.BonusAdditiveMovement = 0;
        }
        else if (type == BonusType.Rotation)
        {
            _playerMover.BonusMultiplicativeRotation = 1;
            _rotationWings.SetActive(false);
        }
        else if (type == BonusType.Wartail)
        {
            _defaultTail.SetActive(true);
            _warTail.SetActive(false);
        }

        _activeRemovers.Remove(type);
    }

    public void ResetBuffs()
    {
        _playerMover.ResetBonuses();
        foreach (var coroutinePair in _activeRemovers)
        {
            StopCoroutine(coroutinePair.Value);
        }
        _activeRemovers.Clear();

        _defaultTail.SetActive(true);
        _warTail.SetActive(false);
        _rotationWings.SetActive(false);

        ResetBuffArray();
    }

    private void ResetBuffArray()
    {
        _buffs = new Dictionary<BonusType, int>
        {
            { BonusType.Movement, 0 },
            { BonusType.Rotation, 0 },
            { BonusType.Wartail, 0 }
        };
    }
}