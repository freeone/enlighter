using UnityEngine;

public class BasicMover : MonoBehaviour
{
    [SerializeField] private float _movementSpeed;
    [SerializeField] private float _rotationSpeed;

    private Vector3 _rotationVector;

    public float BonusAdditiveMovement;
    public float BonusMultiplicativeRotation;

    public float RotationSpeed => _rotationSpeed;

    private void Start()
    {
        ResetBonuses();
        _rotationVector = new Vector3(0, 0, _rotationSpeed);
    }

    public void UpdateMover()
    {
        transform.Translate(transform.up * (_movementSpeed + BonusAdditiveMovement) * Time.smoothDeltaTime, Space.World);
    }

    public void RotateClockWise()
    {
        transform.Rotate(-_rotationVector * BonusMultiplicativeRotation * Time.deltaTime);
    }
    public void RotateCounterclockwise()
    {
        transform.Rotate(_rotationVector * BonusMultiplicativeRotation * Time.deltaTime);
    }

    public void ResetBonuses()
    {
        BonusAdditiveMovement = 0;
        BonusMultiplicativeRotation = 1;
    }
}