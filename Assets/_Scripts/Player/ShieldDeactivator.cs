using UnityEngine;

public class ShieldDeactivator : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.TryGetComponent(out EnemyCollider enemy))
        {
            enemy.DisableShield();
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.TryGetComponent(out EnemyCollider enemy))
        {
            enemy.EnableShield();
        }
    }
}