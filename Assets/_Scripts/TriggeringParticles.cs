using System.Collections.Generic;
using UnityEngine;

public class TriggeringParticles : MonoBehaviour
{
    [SerializeField] private ParticleSystem _particleSystem;
    [SerializeField] private BonusType _type;
    [SerializeField] private Player _player;

    private List<ParticleSystem.Particle> _triggeredParticles;
    private ParticleSystem.Particle _tempParticle;

    private void Awake()
    {
        _triggeredParticles = new List<ParticleSystem.Particle>();
    }

    private void OnParticleTrigger()
    {
        int triggeredCount = 0;
        if (_type == BonusType.Health || _type == BonusType.Defence)
        {
            triggeredCount = _particleSystem.GetTriggerParticles(ParticleSystemTriggerEventType.Enter, _triggeredParticles);
            for (int i = 0; i < triggeredCount; i++)
            {
                _tempParticle = _triggeredParticles[i];
                _tempParticle.remainingLifetime = 0;
                _triggeredParticles[i] = _tempParticle;

                if (_type == BonusType.Health) _player.GetHeal(5);
                else if (_type == BonusType.Defence) _player.AddArmor(5);
            }
            _particleSystem.SetTriggerParticles(ParticleSystemTriggerEventType.Enter, _triggeredParticles);
        }
        else if (_type == BonusType.Gold)
        {
            triggeredCount = _particleSystem.GetTriggerParticles(ParticleSystemTriggerEventType.Inside, _triggeredParticles);
            for (int i = 0; i < triggeredCount; i++)
            {
                _tempParticle = _triggeredParticles[i];

                if (_tempParticle.remainingLifetime < _tempParticle.startLifetime * 0.75f && _tempParticle.remainingLifetime > 0.02f)
                {
                    _tempParticle.remainingLifetime = 0;
                    _triggeredParticles[i] = _tempParticle;
                    _player.ChangeCoinsCount(5);
                }
                _particleSystem.SetTriggerParticles(ParticleSystemTriggerEventType.Inside, _triggeredParticles);
            }
        }
    }
}